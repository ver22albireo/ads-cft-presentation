\documentclass[12pt, a4paper]{article}

% !TeX program = lualatex

\usepackage{pacchetti}
\input{newcommands}

% Bibliografia
\usepackage[backend=bibtex,doi=false,isbn=false,url=false]{biblatex}
\addbibresource{bibliografia.bib}
\renewcommand*{\bibfont}{\fontsize{12}{14.48}\selectfont}

\title{
    \Huge Conformal Field Theory \& Gravity \\
    [1ex]
    \LARGE The Cardy's Formula \\
    \hrulefill \\
    [3ex] \Large Project report
    }
\date{Fall 2022}
\author{Veronica Sacchi\thanks{\href{mailto:veronica.sacchi@epfl.ch}{veronica.sacchi@epfl.ch}}}

\begin{document}

    \maketitle
    \tableofcontents

    \clearpage

    \section{Introduction}
    \label{sec:intro}
    Most known results for physical theories hold at zero temperature, or in the regime of very low \(T\); in general, it is very difficult to go beyond the limited insight given by dimensional analysis (whenever this is even possible), due to the difficulties in resumming the contribuitions from all the states in the spectrum of the theory.  

    It should be rather expected then, that in order to have some control on these regimes, a high level of symmetry is required: beyond Poincar\'e symmetry, one of the most widely and rich symmetry group studied is that of Conformal Symmetry. 
    Notwithstanding the extremely strong constraints imposed in general dimensions, this is not enough yet to fully fix the behaviour of thermodynamics quantities - such as the entropy and the free energy - in the limit of high temperature. 
    
    It is only in two dimensions, where the conformal group expands into an even larger set of local transformations, that we gain enough control on the (unitary) theories' spectra, so to fully predict the leading behaviour of thermodynamic potentials regardless of the details of the theory.

    In the following, we are going to review in great detail this general scheme of ideas, by specifically studying the entropy density in general dimension, and then deriving the famous Cardy's formula (when \(d = 2 \)). 

    Finally, we will try to review how much this result has to tell us about the physics of CFTs, and to understand how its large regime of validity can be exploited to gain further intuition in related contexts, such as holography.

    \section{Quantum Field Theory at finite temperature}
    \label{sec:finite-temp}
    \begin{wrapfigure}{R}{0.33\textwidth}
        \begin{center}
        \begin{tikzpicture}[scale=0.5]
            \draw[densely dashdotted, fuchsia] (0,0) ellipse (1.7 and 0.5);
            \draw[turquoise, thick] (-1.7, 0) -- (-1.7, 7);
            \draw[turquoise, thick] (1.7, 0) -- (1.7, 7);
            \draw[densely dashdotted, fuchsia] (0,7) ellipse (1.7 and 0.5);
            \draw[turquoise, ->] (2.33, 2.9) -- (2.33, 0);
            \draw[turquoise, ->] (2.33, 4.1) -- (2.33, 7);
            \node[turquoise, right] at (1.8, 3.5) {\(\tilde{t}\)};
            \node[fuchsia, above] at (0, 0.5) {\(\phi_1\)};
            \node[fuchsia, below] at (0, 6.5) {\(\phi_2\)};
        \end{tikzpicture}
        \end{center}
        \caption{Pictorial representation of the path integral computing the Lorentzian transition amplitude from \(\phi_1\) to \(\phi_2\).}
        \label{fig:lorentzian-path-integral}
    \end{wrapfigure}

    First of all, it would be important to understand what we mean by ``studying a QFT at finite temperature''. The result that we will try to motivate in this section is usually summarised by the prescription of ``applying a Wick rotation and compactify the Euclidean time''.

    To make sense of such a statement, let's first recall what the Path Integral is meant to represent: transition amplitudes from a field configuration \(\phi_1\) to another field configuration \(\phi_2\).

    \[
        \langle\phi_2 \vert e^{-i\tilde{t}H}\vert \phi_1\rangle \\
        = \int_{\phi(t=0) \equiv \phi_1}^{\phi(t=\tilde{t}) \equiv \phi_2}\left[\mathcal{D}\phi\right]e^{iS}. 
    \]
    Figure~\ref{fig:lorentzian-path-integral} displays the space where the path integral should be computed, in the specific case where the QFT lives on a compact space (for non-compact space it would simply be a strip).

    As a second element, recall that in statistical physics, studying a system at finite temperature means that we should expect the system to be in some state with a probability distribution that depends on the energy of the state itself, known as Boltzmann distribution. To average over all the (usually infinite) states we now need to account for, it is ``enough'' to re-sum the partition function \(\mathcal{Z}\), from which any macroscopic quantity can be derived.

    Finally, to merge the two together, replace the \emph{states} of statistical physics with \emph{field configurations} and notice the similarity of the definition of \(\mathcal{Z}\) with the amplitudes computed by the Path integral:

    \[
        \mathcal{Z} = \texttt{tr}e^{-\beta H} = \sum_{\phi_1} \langle\phi_1 \vert e^{-\beta H}\vert \phi_1\rangle.
    \]

    In this case however, the ``time evolution'' appearing within the bra and the ket is Euclidean, from which the need of the Wick rotation, and the identification of the Euclidean time with the temperature's inverse. Moreover, both the bra and the ket refer to the \emph{same} state, over which we sum, so we are literally gluing the two ends of the Euclidean cylinder over which the Path integral is computed. Figure~\ref{fig:compactify-euclidean-time} shows this procedure in the case of a QFT living on a compact space.

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.44]
            \draw[densely dashdotted, fuchsia] (0,0) ellipse (1.7 and 0.5);
            \draw[turquoise, thick] (-1.7, 0) -- (-1.7, 7);
            \draw[turquoise, thick] (1.7, 0) -- (1.7, 7);
            \draw[densely dashdotted, fuchsia] (0,7) ellipse (1.7 and 0.5);
            \draw[turquoise, ->] (2.33, 2.9) -- (2.33, 0);
            \draw[turquoise, ->] (2.33, 4.1) -- (2.33, 7);
            \node[turquoise, right] at (1.8, 3.5) {\(\beta\)};
            \node[fuchsia, above] at (0, 0.5) {\(\phi_1\)};
            \node[fuchsia, below] at (0, 6.5) {\(\phi_1\)};
            \draw[ultra thick, goldenyellow, ->] (3, 3.8) arc(150:30:1.1);
            \draw[turquoise, thick, looseness=6] (9.3, 2) to[out=-114, in=114] (9.3, 4);
            \draw[turquoise, thick] (12.7, 4) arc(17:343:3.7);
            \draw[densely dashdotted, fuchsia, thick] (11,2) ellipse (1.7 and 0.5);
            \draw[densely dashdotted, fuchsia, thick] (11,4) ellipse (1.7 and 0.5);
            \node[fuchsia, below] at (11, 1.5) {\(\phi_1\)};
            \node[fuchsia, above] at (11, 4.5) {\(\phi_1\)};
            \draw[ultra thick, goldenyellow, ->] (12.7, 0.8) arc(200:330:1.1);
            \draw[black, densely dashed] (17.5, 0.85) ellipse (0.7 and 1.75);
            \draw[turquoise, thick] (17.5, 3.1) ellipse (3 and 4);
            \draw[turquoise, thick] (16.5, 3.1) arc(210:330:1.1);
            \draw[turquoise, thick] (16.9, 2.8) arc(150:30:0.7);
            \node[black] at (17.2, 0.85) {\(\beta\)};
        \end{tikzpicture}
        \caption{The sum over all field configurations corresponds to gluing the \(2\) ends of the cylinder, so to obtain an Euclidean torus if we were starting from a \(1+1\) QFT on a compact space.}
        \label{fig:compactify-euclidean-time}
    \end{figure}
    

    \section{Thermodynamic potentials}
    \label{sec:td-potentials}
    As anticipated in the previous section, in general it is very difficult to extract any physical insight about the large temperature behaviour of any system. However, in the case of a generic CFTs, dimensional analysis can already provide a deep breakthrough.

    In order to show this, let's start by considering a generic QFT (not necessary conformal for now), living on a cylinder \(S^{d-1} \times I\), where \(I\) represents the time interval (while \(S^{d-1}\) is composed by the \emph{compact} space directions). It is a very general prescription that, analysing a Lorentzian theory at finite temperature is equivalent to studying the (Wick rotated) theory with a compactified Euclidean time (for more details see section \ref{sec:finite-temp}). 

    Therefore, the Euclidean action will be given by an integral of the form:
    \[
    S_E = \int_{S^{d-1}} d^{d-1}x\int_{S^1} dt_E \mathcal{L} [\varphi_i(t_E, \vec{x})];  
    \]
    when imposing periodic boundary conditions on the spatial direction, we are able to decompose all the fields that populate the theory in Fourier modes:
    \[
    \varphi_i (t_E, \vec{x}) = \sum_{\vec{n}} e^{\frac{2i\pi}{R}\vec{n}\cdot\vec{x}} \hat{\varphi}^{(\vec{n})}_i(t_E),
    \]
    so the integral over the spatial directions will select only the combinations of modes that give a vanishing total momentum, and will factor out the volume contribution \(R^{d-1} Vol\left(S^{d-1}\right)\):

    \[
    S_E = R^{d-1} Vol\left(S^{d-1}\right) \cdot \int_{S^1} dt_E L[\hat{\varphi}^{(\vec{n})}_i(t_E)].
    \]

    Henceforth, the partition function as well can be decomposed as 
    \[
    \mathcal{Z} = \texttt{tr}\left[e^{-\beta H}\right]   = \texttt{tr}\left[e^{-R^{d-1} Vol\left(S^{d-1}\right)\frac{1}{T}\mathcal{H}}\right],
    \]
    and by dimensional analysis, since the exponenent needs to have dimension \(0\) and the only adimensional combination is \(RT\), we get:
    \[
        \mathcal{Z} =  \texttt{tr}\left[e^{-Vol\left(S^{d-1}\right)(RT)^{d-1} g(RT)}\right],
    \]
    where the function \(g\) incorporates the sum over all the Fourier modes.

    Now, if the theory is conformally invariant, \(g\) will be the same regardless of the size of radius of the ball, and in particular, by taking the limit of \(R\rightarrow +\infty\) we can retrive the intensive free energy for the plane:
    
    \[
    f_{\R^d} = -\lim_{R\rightarrow +\infty}\frac{1}{\beta V}  \ln\mathcal{Z} =  -T^d \lim_{R\rightarrow +\infty} g(RT) = -g_{\infty}T^d.
    \]

    Consequently, from the free energy differential \(dF = -SdT - PdV\), we can derive the intensive entropy
    \begin{equation}
        \label{eq:intensive-s}
        s = -\frac{1}{V}\frac{\delta F}{\delta T}\Big\vert_{V} = - dg_{\infty}T^{d-1}. 
    \end{equation}

    We see that the general temperature scaling of thermodynamic potentials in conformal field theories is fully fixed by dimensional analysis, however at this level very little can be said about the coefficient of this leading term. The purpose of the following work will be precisely to show how in \(2\) dimensions such coefficient can be computed in a very elegant and rigorous way, for a very wide set of theories. In order to prove this, we will mainly refer to section \(25\) of the Lecture notes by T. Hartman~\cite{hartman-lectures}.

    \section{The Modular Group}
    \label{sec:modular-group}

    We argued in section~\ref{sec:finite-temp} how studying a \((1+1)\)-CFT on a compact space corresponds to studying the corresponding Euclidean CFT on a torus, where one of the dimensions has periodicity \(\beta\).

    A torus is usually defined via the quotient of the plane \(\R^2\) with \(2\) vectors \(v_1\) and \(v_2\). By parameterizing the plane with a complex coordinate \(z\), this corresponds to stating 
    \[
    z \sim z + v_1 \sim z + v_2 \left(\sim z + v_1 + v_2\right),    
    \]
    and the parallelogram of sides \(v_1\) and \(v_2\) will be regarded as the \emph{fundamental domain} of our quotient.

    Any QFT which claims any physical relevance enjoys Lorentz Invariance, and so the corresponding Euclidean theory will be invariant under rotations: we can rotate the two vectors so that \(v_2\) lies on the spatial axis, while, if we are also blessed with scale invariance, we can rescale the full system so that \(v_2 = 2\pi\). In short, this is shown in figure~\ref{fig:rotation-torus}.
    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.7]
            \draw[fuchsia, densely dashdotted] (0,0) -- (4.2, 1.4);
            \draw[fuchsia, densely dashdotted] (0,1.33) -- (4.2, 2.77);
            \draw[fuchsia, densely dashdotted] (0,2.66) -- (4.2, 4.10);
            \draw[turquoise, densely dashed] (0,0) -- (2,4);
            \draw[turquoise, densely dashed] (1.25,0) -- (3.25,4);
            \draw[turquoise, densely dashed] (2.5,0) -- (4.5,4);
            \draw[turquoise, densely dashed] (0,2.5) -- (0.75,4);
            \draw[black, ->] (-0.1, 0) -- (4,0) node[below] {\(x_E\)};
            \draw[black, ->] (0,-0.1) -- (0, 4.2) node[left] {\(t_E\)};
            \draw[thick, fuchsia, ->] (0,0) -- (1.5, 0.5) node[midway, above] {\(\vec{v}_2\)};
            \draw[thick, turquoise, ->] (0,0) -- (0.8, 1.6) node[left] {\(\vec{v}_1\)};
            \draw[ultra thick, goldenyellow, ->] (4.3, 2) arc(140:45:1.5);
            \draw[fuchsia, densely dashdotted] (7,2) -- (11.5, 2);
            \draw[fuchsia, densely dashdotted] (7,4) -- (11.5, 4);
            \draw[turquoise, densely dashed] (7,0) -- (11,4);
            \draw[turquoise, densely dashed] (7,2.5) -- (8.5,4);
            \draw[turquoise, densely dashed] (9.5,0) -- (11.5,2);
            \draw[black, ->] (6.9, 0) -- (11,0) node[below] {\(x_E\)};
            \draw[black, ->] (7,-0.1) -- (7, 4.2) node[left] {\(t_E\)};
            \filldraw[black] (9.5, 0) circle (2pt) node[below] {\(2\pi\)};
            \draw[thick, fuchsia, ->] (7,0) -- (9.5, 0) node[midway, above] {\(\vec{v}_2\)};
            \draw[thick, turquoise, ->] (7,0) -- (9, 2) node[midway, above] {\(\vec{v}_1\)};
        \end{tikzpicture}
        \caption{for CFTs we can always rotate and rescale so to have \(v_2=2\pi\). This immediately shows that there really are only \(2\) degrees of freedom to define the torus.}
        \label{fig:rotation-torus}
    \end{figure}

    It is clear at this point that there are at most only \(2\) degrees of freedom which defines the torus (we only need to determine \(v_1\)): the relevant quantity is called \emph{modulus} and is defined as:
    \[
    \tau \coloneqq \frac{v_1}{v_2} = \frac{\theta}{2\pi} + i\frac{\beta}{2\pi}.   
    \]
    As we anticipated above, \(\beta\) will be identified with the inverse temperature, while \(\theta\) is the angular momentum: for simplicity we will work with \(\theta = 0\), but similar proofs can be worked out for generic \(\theta\), at the price of slightly more complicated formulas.

    Up to now we have pointed out how, for each modulus a torus -- and so a CFT -- is defined, but it is not clear at all whether different moduli should necessarily correspond to some different CFTs. The answer is given by a well-known mathematical fact which states that two moduli \(\tau\) and \(\tau'\) will give rise to the same complex structure (and so to the same CFT), if and only if they are related via a modular transformation.

    The statement of this theorem is provided in many places, among others a detailed proof can be found in \cite{nakahara2003physics}.

    Instead of showing the rigorous proof, which is not the aim of this work, let us try to build an intuition of why modular transformations shouldn't change the underlying CFT. The most general modular transformation looks like:
    \[
        \tau \mapsto \frac{a\tau + b}{c\tau + d}  \quad\quad ad - bc = 1,
    \]
    where the coefficients are necessarily integers, \(a,b,c,d \in \mathbb{Z}\).

    The group has \(2\) generators, commonly known as \(T\) and \(S\). The first one is ``simply'' a translation of \(v_1\) by \(1\):
    \[
    T: \tau \mapsto \tau+1.
    \]

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.7]
            \draw[fuchsia, densely dashdotted] (0,1.7) -- (4.2, 1.7);
            \draw[fuchsia, densely dashdotted] (0,3.4) -- (4.2, 3.4);
            \draw[turquoise, densely dashed] (0,0) -- (4.2,4.2);
            \draw[turquoise, densely dashed] (1.5,0) -- (4.5,3);
            \draw[turquoise, densely dashed] (3,0) -- (4.5,1.5);
            \draw[turquoise, densely dashed] (0,1.5) -- (2.7,4.2);
            \draw[turquoise, densely dashed] (0,3) -- (1.2,4.2);
            \draw[black, ->] (-0.1, 0) -- (4.2,0) node[below] {\(x_E\)};
            \draw[black, ->] (0,-0.1) -- (0, 4.6) node[left] {\(t_E\)};
            \draw[thick, fuchsia, ->] (0,0) -- (1.5, 0) node[midway, below] {\(\vec{v}_2\)};
            \draw[thick, turquoise, ->] (0,0) -- (1.7, 1.7) node[midway, left] {\(\vec{v}_1\)};
            \draw[ultra thick, goldenyellow, ->] (4.3, 2) arc(140:45:1.5);
            \begin{scope}[xshift=7cm]
                \fill[goldenyellow] (0,0) -- (1.7,1.7) -- (3.2, 1.7) --cycle;
                \fill[goldenyellow] (1.5,0) -- (3.2,1.7) -- (4.7, 1.7) --cycle;
                \draw[fuchsia, densely dashdotted] (0,1.7) -- (4.7, 1.7);
                \draw[fuchsia, densely dashdotted] (0,3.4) -- (4.7, 3.4);
                \draw[black, densely dashed] (0,0) -- (4.2,4.2);
                \draw[black, densely dashed] (1.5,0) -- (4.5,3);
                \draw[black, densely dashed] (3,0) -- (4.5,1.5);
                \draw[black, densely dashed] (0,1.5) -- (2.7,4.2);
                \draw[black, densely dashed] (0,3) -- (1.2,4.2);
                \draw[turquoise, densely dashed] (0,0) -- (4,2.125);
                \draw[turquoise, densely dashed] (1.5,0) -- (4.7,1.7);
                \draw[turquoise, densely dashed] (3,0) -- (4.2,0.6);
                \draw[turquoise, densely dashed] (0,0.76) -- (4,3);
                \draw[turquoise, densely dashed] (0,1.55) -- (4,3.675);
                \draw[black, ->] (-0.1, 0) -- (4.2,0) node[below] {\(x_E\)};
                \draw[black, ->] (0,-0.1) -- (0, 4.6) node[left] {\(t_E\)};
                \draw[thick, fuchsia, ->] (0,0) -- (1.5, 0) node[midway, below] {\(\vec{v}_2\)};
                \draw[thick, turquoise, ->] (0,0) -- (3.2, 1.7) node[above] {\(\vec{v}'_1\)};
            \end{scope}
        \end{tikzpicture}
        \caption{action of the \(T\) generator on the complex structure.}
        \label{fig:T}
    \end{figure}
    Figure~\ref{fig:T} depicts what the action of \(T\) looks like; in particular, it corresponds to gluing the left yellow triangle, belonging to the fundamental domain containing \(2\pi\) before the transformation, to the right that same domain, becoming the right yellow triangle. It is evident then, that we are describing exactly the same structure (the periodicities haven't changed), we are simply parameterizing it differently. As usual, physics doesn't care of the parameterization we choose to describe her, henceforth \emph{any} quantum field theory (it doesn't need to be a CFT) will be invariant under \(T\).

    For \(S\) the story is a little bit different; its action is given by:
    \[
    S: \tau \mapsto -\frac{1}{\tau}    
    \]
    which corresponds to an inversion followed by a reflection on the real axis on the complex plane. Trying to depict a representation of the transformation (see figure \ref{fig:S}) we can realise that \(S\) basically \emph{swaps} the vectors \(v_1\) and \(v_2\), but in order to compare the new fundamental domain with the old one we need to rescale \(v_1'\) down to \(2\pi\). This is a clear signal that \(S\) cannot be a symmetry of a general QFT, but needs scale invariance to be included.

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale = 1]
            \draw[black, ->] (-0.1, 0) -- (3,0) node[below] {\(x_E\)};
            \draw[black, ->] (0,-0.1) -- (0, 3) node[left] {\(t_E\)};
            \draw[thick, fuchsia, ->] (0,0) -- (1.5, 0) node[midway, below] {\(\vec{v}_2\)};
            \draw[thick, turquoise, ->] (0,0) -- (1.7, 1.7) node[midway, above] {\(\vec{v}_1\)};
            \draw[ultra thick, goldenyellow, ->] (2.3, 1.5) arc(140:45:1.5);
            \begin{scope}[xshift=5cm]
                \draw[black, ->] (-0.1, 0) -- (3,0) node[below] {\(x_E\)};
                \draw[black, ->] (0,-0.1) -- (0, 3) node[left] {\(t_E\)};
                \draw[thick, fuchsia, ->] (0,0) -- (1.06, 1.06) node[above] {\(\vec{v}_1\)};
                \draw[thick, turquoise, ->] (0,0) -- (2.41, 0) node[midway, below] {\(\vec{v}_2\)};
            \end{scope}
        \end{tikzpicture}
        \caption{action of the \(S\) generator on the complex plane.}
        \label{fig:S}
    \end{figure}

    \section{Thermodynamics in 2 dimensions}
    \label{sec:td-2d}
    We concluded the previous section arguing that \(S\) is a symmetry for any CFT living on the \(2-\)torus. For \(\theta = 0\) the transformation acts like:
    \[
       i\frac{\beta}{2\pi} = \tau \mapsto - \frac{1}{\tau} = \frac{2\pi}{i\beta} = - i \frac{1}{2\pi}\left(\frac{4\pi^2}{\beta}\right) 
    \]
    We immediately see that this estabilshes a duality between the high and the low temperature regimes:
    \[
    S-\text{duality}: \quad\quad \mathcal{Z}(\beta) = \mathcal{Z}\left(\frac{4\pi^2}{\beta}\right).
    \]
    Dualities such as this one are extremely helpful, because they relate regimes where very little is known, as any ``perturbative'' approach breaks down, to regimes where instead we are very comfortable.

    As the high temperature regime of the former theory is dual to the low temperature regime of the \(S-\)transformed one, we can easily estimate the partition function by truncating the series to the first term:

    \[
    T\rightarrow +\infty: \quad\quad  \mathcal{Z}(\beta) =\mathcal{Z}\left(\frac{4\pi^2}{\beta}\right) = \sum_n e^{-\frac{4\pi^2}{\beta}E_n} \simeq e^{-\frac{4\pi^2}{\beta}E_0}
    \]

    The relevant spectra is the one of a CFT on the cylinder (we started with the assumption of a compact space), and for that it is well known (see \cite{francesco2012conformal} for a detailed derivation) that the ground state has energy 
    \[
    E_0 = -\frac{c}{12}. 
    \]
    This is due to the fact that the stress tensor is not a primary operator, and so under Weyl transformation (such as the one relating the plane and the cylinder) picks an extra term due to the Schwartzian derivative of the map. 

    This state corresponds to the identity in the state operator correspondence (the only one with conformal dimension \(\Delta = 0\)), and it should be pointed out that here is where we are implicitly assuming unitarity of the theory: if there were states with negative conformal dimension, according to the formula
    \[
    E_{\Delta} = \Delta - \frac{c}{12},   
    \]
    the ground state would be some state with \(\Delta < 0\), but it would correspond to an operator whose \(2-\)point correlation function would grow at large distances, instead of getting suppressed.

    Now that the partition function has been finally computed we can derive the entropy and the internal energy via some established thermodynamic identities:
    \begin{align*}
        S = \left(1 - \beta\frac{\partial}{\partial\beta}\right)\ln\mathcal{Z} &= \frac{2\pi^2c}{3\beta} \\
        E = -\frac{\partial}{\partial\beta}\ln\mathcal{Z} &= \frac{\pi^2c}{3\beta^2} \\
        &\Big\Downarrow \\
         S(E) = 2\pi &\sqrt{\frac{c}{3}E}
    \end{align*}
    The latter is what is typically known as ``Cardy's Formula''. Comparing the intensive entropy (recall that here we fixed the spatial volume to be \(2\pi\))
    \[
    s = \frac{S}{2\pi} = \frac{\pi}{3}cT 
    \]
    with the formula~\eqref{eq:intensive-s}, we obtain the coefficient 
    \[
        g_{\infty} = - \frac{\pi}{6},
    \]
    so we really have completely fixed the leading term of the entropy behaviour at high temperatures.
    \section{Further developments}
    \label{sec:furth-devs}
    The Cardy's formula was discovered by the homonymous physicist in \(1985\) (see \cite{cardy1986operator} for the original paper), but its applications are still under investigation nowadays, due to its incredibly vast domain of applicability. 
    % For example, it opens the way to an actual experimental measure of the central charge of a \(CFT_2\), by simply computing the coefficient of the log-term of Entanglment entropy. 
    
    Subleading contributions to the entropy behaviour can be computed by including more states of the spectrum, which has been done in \cite{mukhametzhanov2019modular}, where the authors also find a bound on the ``sparseness'' of Virasoro theories via bounds on the entropy correction. Finally, extensions to higher dimensions have been attempted, even if in this case there won't be any such general formula and the coefficient \(g_{\infty}\) might depend on other terms rather than just \(c\). 
    
    However, the most widely known application of this work is due to Strominger~\cite{strominger1998black}, who in \(1997\) realised that the Entropy of BTZ black holes was given precisely by the Cardy's formula, with a central charge of Brown-Hennaux. They proved \cite{brown1986central} that any quantum gravity theory in \(\ell-AdS_3\) must be equivalent to a \(CFT_2\) with central charge:
    \[
    c = \frac{3\ell}{2G},   
    \]
    and Strominger used it to show that the Bekenstein-Hawking entropy of these asymptotically \(AdS_3\) black hole solutions coincides with the CFT prediction.
    
    This result had a very important impact historically, since it finally laid to rest all doubts that the Black Hole Area law was really to be intended as a thermodynamic identity, rather just being accidentally similar to it.

    \printbibliography

\end{document}